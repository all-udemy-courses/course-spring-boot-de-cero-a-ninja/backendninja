package com.course.backendninja.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/say")
public class HelloWorldController {

    private static final String HELLOWORLD_VIEW = "helloworld";

    @GetMapping("/v1/helloworld")
    public String helloWorldV1(){
        return HELLOWORLD_VIEW;
    }

    @GetMapping("/v2/helloworld")
    public ModelAndView helloWorldV2(){
        return new ModelAndView(HELLOWORLD_VIEW);
    }
}
